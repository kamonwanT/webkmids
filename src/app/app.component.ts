import { StudentService } from './student.service';
import { Component } from '@angular/core';
import { Student } from './student.model';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  students: Student[] = [];
  studentForm: FormGroup;
  isFormReady = false;
  
  constructor() {
    
  }
}
