import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { StudentService } from './student.service';
import { StudentComponent } from './student/student.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterfromComponent } from './registerfrom/registerfrom.component';
import { RegisterfromcreateComponent } from './registerfromcreate/registerfromcreate.component';


export const routes: Routes =[
  {path:'', redirectTo:'/student', pathMatch: 'full'},
  {path:'student', component:StudentComponent},
  {path:'registerfrom', component:RegisterfromComponent},
  {path:'registerfromcreate', component:RegisterfromcreateComponent},
  
];

@NgModule({
  declarations: [
    AppComponent,
    StudentComponent,
    RegisterfromComponent,
    RegisterfromcreateComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
