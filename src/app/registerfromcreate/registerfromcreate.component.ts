import { StudentParent } from './../student.model';
import { Component, OnInit } from '@angular/core';
import { Student } from '../student.model';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { StudentService } from '../student.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registerfromcreate',
  templateUrl: './registerfromcreate.component.html',
  styleUrls: ['./registerfromcreate.component.css']
})
export class RegisterfromcreateComponent implements OnInit {
  selectedFile = []
  students: Student[] = [];
  studentForm: FormGroup;
  parentFormArray: FormArray;
  isFormReady = false;
  relationdata: any
  countrydata: any
  citydata: any
  img:any = null

  constructor(private studentService: StudentService, private router: Router, private http: HttpClient, private formBuilder: FormBuilder) {
    this.studentService.getAllStudent().subscribe(result => {
      console.log(result)
      this.students = result.data;
      // student
      this.parentFormArray = this.formBuilder.array([]);
      
      const newStudent = {studentCode:'' ,firstName: '',lastName:'',middleName:'',idCard:'',email:'',
      phone:'',cardId:'',imageName:'',admitYear:'',entryGrade:'',currentGrade:'',currentRoom:'',addressId:'',
      studentParents:this.parentFormArray }
      this.studentForm = this.formBuilder.group(newStudent);
      //address
      const newaddress = {address1:'' ,address2: '',cityId:'',zipCode:'',cityCode:'',cityName:'',
      countryId:'',countryCode:'',countryName:''}
      this.studentForm.setControl('address' , this.formBuilder.group(newaddress));
      this.isFormReady = true;
      console.log(this.studentForm);

    });
  }
  get studentParents(): FormArray { return this.studentForm.get('studentParents') as FormArray; }
  senddata() { 
    console.log(this.studentForm.value);
   }
   

  ngOnInit() {
    this.city()
    this.country()
    this.relation()
  }
 create(){
    console.log(this.studentForm.getRawValue());
    this.studentService.createStudent(this.studentForm.getRawValue()).subscribe(result=>{
      console.log(result);
      alert('Success');
      this.router.navigate(['student'])
    });
  }
  country(){
    var data = {
      'apiRequest': { 'action': 'get_all' }
    }
    this.http.post('http://kmids.ctt-center.com/api/country', data)
      .subscribe((res: any) => {
        this.countrydata = res.data
        console.log(res.data);
      })
  }
  city(){
    var data = {
      'apiRequest': { 'action': 'get_all' }
    }
    this.http.post('http://kmids.ctt-center.com/api/city', data)
      .subscribe((res: any) => {
        this.citydata = res.data
        console.log(res.data);
      })
  }
  relation(){
    var data = {
      'apiRequest': { 'action': 'get_all' }
    }
    this.http.post('http://kmids.ctt-center.com/api/relation', data)
      .subscribe((res: any) => {
        this.relationdata = res.data
        console.log(res.data);
      })
  }
  Back(){
    this.router.navigate(['student'])
   }
   addParent() {
    //const control = <FormArray>this.studentForm.controls['studentParents'];
     // const newparent = {relationId:'',firstName: '',lastName:'',middleName:'',idCard:'',email:'',phone:'',cardId:'',imageName:''}
     // const newParent = this.newParent();
     const parentFormGroup = this.formBuilder.group({
      firstName: '',
      lastName: '',
      middleName: '',
      phone: '',
      cardId: '',
      idCard: '',
      email: '',
      imageName: '',
      addressId: ''

    });
    const studentParentFormGroup = this.formBuilder.group({
      studentId: '',
      relationId: '',
      parentId: '',
      parent: parentFormGroup
    });
     // const parentGroup = this.formBuilder.group(studentParent);
     const control = <FormArray>this.studentForm.controls['studentParents'];
     control.push(studentParentFormGroup);
     console.log(this.studentForm);
   }
   
   newParent() {
     
     return parent;
   }
   
  onUploadimg(event) {
    
    console.log(event)

    this.selectedFile = event.target.files;
    console.log(this.selectedFile)
    let fd = new FormData();
    for (let index = 0; index < this.selectedFile.length; index++) {

      var img = fd.append('image', this.selectedFile[index], this.selectedFile[index].name)
    }
    // fd.append('image', this.selectedFile, this.selectedFile.name)

    this.http.post('http://kmids.ctt-center.com/api/Upload', fd).subscribe((x: any) => {

      // (<FormArray>this.form.get('image')).push(this.getCreateImage(x.data))
      // console.log(this.form.get('image').value)
      console.log(x)
      x.data.forEach(element => {
        console.log(element.name);
        this.img = element.name
      }
      );
    }
    )
  }

}
