import { CoreApiResponse } from './api.model';
export interface StudentResponse extends CoreApiResponse<Student> {
}
export interface Student {
    studentCode: string;
    firstName: string;
    lastName: string;
    middleName: string;
    idCard: string;
    email: string;
    phone: string;
    cardId: string;
    imageName: string;
    admitYear: string;
    entryGrade: string;
    currentGrade: string;
    currentRoom: string;
    addressId: string;
    address: Address;
    id: string;
    studentParents: StudentParent[];
}
// export class Student2 {
//     studentCode: string;
//     firstName: string;
//     lastName: string;
//     middleName: string;
//     idCard: string;
//     email: string;
//     phone: string;
//     cardId: string;
//     imageName: string;
//     admitYear: string;
//     entryGrade: string;
//     currentGrade: string;
//     currentRoom: string;
//     addressId: string;
//     address: Address;
//     id: string;
//     constructor() {
//         this.studentCode='';
//     }
//     studentParents: StudentParent[];
// }
export interface StudentParent {
    parent: Parent;
}
export interface Parent {
    firstName: string;
    lastName: string;
    middleName: string;
    idCard: string;
    email: string;
    phone: string;
    cardId: string;
    imageName: string;
    addressId: Address;
    address: Address;
}
export interface Address {
    address1: string;
    address2: string;
    cityId: string;
    zipCode: string;
    cityCode: string;
    cityName: string;
    countryId: string;
    countryCode: string;
    countryName: string;
}
