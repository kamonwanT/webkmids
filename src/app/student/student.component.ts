import { Component, OnInit, Input } from '@angular/core';
import { StudentService } from './../student.service';
import { Student } from './../student.model';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  students: Student[] = [];
  studentForm: FormGroup;
  
  constructor(private studentService: StudentService,
    private router: Router, private http: HttpClient, private formBuilder: FormBuilder,) {
    this.studentService.getAllStudent().subscribe(result => {
      console.log(result);
      this.students = result.data;
      
    });
  }

  ngOnInit() {
    this.studentService.getAllStudent().subscribe(result => {
      console.log(result);
      this.students = result.data;
      // alert('Success');
    });
  }

  update(id) {
    console.log(id)
    this.router.navigate(['registerfrom'])
    this.studentService.setData(id)
  }
 addstudent(){
  this.router.navigate(['registerfromcreate'])
 }
  remove(Student){
    this.studentService.removeStudent(Student).subscribe(result=>{
      console.log(result);
      alert('Delete Data!')
      this.ngOnInit()
    });
  }
}
