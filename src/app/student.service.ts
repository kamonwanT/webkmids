import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StudentResponse, Student } from './student.model';

@Injectable()
export class StudentService {
    sharedData :any
    constructor(private httpClient: HttpClient) {
    }
    setData(data){
        this.sharedData = data
        
    }
    getData(){
        console.log(this.sharedData)
        return this.sharedData
    }
    getAllStudent(): Observable<StudentResponse> {
        // Javascript object
        const request = { apiRequest: { action: 'get_tree'}, data: [{}]
        };
        return this.httpClient.post<StudentResponse>('http://kmids.ctt-center.com/api/student', request);
    }
    createStudent(student:Student): Observable<StudentResponse> {
        // Javascript object
        var addstudent =  [student];
        const request = { apiRequest: { action: 'create_tree'}, data: addstudent
        };
        return this.httpClient.post<StudentResponse>('http://kmids.ctt-center.com/api/student', request);
    }
    updateStudent(student:Student): Observable<StudentResponse> {
        // Javascript object
        var updatestudents =  [student];
        const request = { apiRequest: { action: 'update_tree'}, data: updatestudents
        };
        return this.httpClient.post<StudentResponse>('http://kmids.ctt-center.com/api/student', request);
    }
    removeStudent(student:Student): Observable<StudentResponse> {
        // Javascript object
        const request = { apiRequest: { action: 'remove_tree'}, data: [{id: student.id}]
        };
        return this.httpClient.post<StudentResponse>('http://kmids.ctt-center.com/api/student', request);
    }
}

// getAllotmentDate(): Observable<AllotmentDateResponse> {
//     const request = {
//       apiRequest: {
//         action: 'get'
//       },
//       data: [
//             {
//               forDate: '2018-09-30'
//             }
//         ]
//     };
//     return this.http.post<AllotmentDateResponse>('http://pms.thamming.com/papi/sales/allotmentdate', request);
