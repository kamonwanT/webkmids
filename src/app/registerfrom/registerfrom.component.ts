import { Component, OnInit } from '@angular/core';
import { Student } from '../student.model';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { StudentService } from '../student.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registerfrom',
  templateUrl: './registerfrom.component.html',
  styleUrls: ['./registerfrom.component.css']
})
export class RegisterfromComponent implements OnInit {
  students: Student[] = [];
  studentForm: FormGroup;
  isFormReady = false;
  countrydata: any
  citydata: any

  studentsData:any = []

  constructor(private studentService: StudentService,private router: Router, private http: HttpClient, private formBuilder: FormBuilder) {
    this.studentService.getAllStudent().subscribe(result => {
      console.log(result)
      this.students = result.data;
      this.studentForm = this.formBuilder.group(result.data[0]);
      this.studentsData = this.students.find(f => f.id == this.studentService.getData())
      console.log(this.studentsData)
      this.studentForm.patchValue({studentCode:this.studentsData.studentCode ,
        firstName:this.studentsData.firstName,
        lastName:this.studentsData.lastName,
        middleName:this.studentsData.middleName,
        idCard:this.studentsData.idCard,
        email:this.studentsData.email,
        phone:this.studentsData.phone,
        cardId:this.studentsData.cardId,
        imageName:this.studentsData.imageName,
        admitYear:this.studentsData.admitYear,
        entryGrade:this.studentsData.entryGrade,
        currentGrade:this.studentsData.currentGrade,
        currentRoom:this.studentsData.currentRoom,
        addressId:this.studentsData.addressId})
        
      // Update
      this.studentForm.setControl('address' , this.formBuilder.group(this.studentsData.address));

      const arrayOfFormGroup = result.data[0].studentParents.map(data => {
        const group = this.formBuilder.group(data);
        group.setControl('parent', this.formBuilder.group(data.parent));
        console.log(group);
        return group; });

      this.studentForm.setControl('studentParents', this.formBuilder.array(arrayOfFormGroup));
      // this.studentForm.setControl('studentParents', this.formBuilder.array(result.data[0].studentParents));
      // this.studentForm.addControl('address' , this.formBuilder.group(result.data[0].address));
      this.isFormReady = true;
      console.log(this.studentForm);

    });
  }
  get studentParents(): FormArray { return this.studentForm.get('studentParents') as FormArray; }
  senddata() {
    console.log(this.studentForm.value);
   }

  ngOnInit() {
    this.city()
    this.country()
    console.log(this.students)
}
Back(){
  this.router.navigate(['student'])
 }
  update(Student)
  {
    console.log(this.studentForm.getRawValue());
    this.studentService.updateStudent(Student).subscribe(result=>{
      console.log(result);
      alert('Success');
    });
  }
  country(){
    var data = {
      'apiRequest': { 'action': 'get_all' }
    }
    this.http.post('http://kmids.ctt-center.com/api/country', data)
      .subscribe((res: any) => {
        this.countrydata = res.data
        console.log(res.data);
      })
  }
  city(){
    var data = {
      'apiRequest': { 'action': 'get_all' }
    }
    this.http.post('http://kmids.ctt-center.com/api/city', data)
      .subscribe((res: any) => {
        this.citydata = res.data
        console.log(res.data);
      })
  }
}
